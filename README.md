# Hotel APIs
API for administration of visits in a hotel. Only ```Python 3.x```

# Architecture API

![alt text](http://spyder-sec.com/imgs_radom/cap.png)

# Setting
Configure virtual environment
```bash
$ virtualenv -p python3 env3
```

Active virtual environment
```bash
$ source env3/bin/activate
```

Install package modules
```bash
$ (env3) pip install --upgrade -r requirements.txt
```

Setup environment variables
```bash
$ (env3) source .env
```

# Use
Start project :D
```bash
$ (env3) python manage.py runserver 8080
```

# For Cloud Firestore

## Visitors API
<strong>GET</strong> - Visitor list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/visitors/

<strong>POST</strong> - Visitor list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/visitor/<br>
<strong>Parameters:</strong>
- event_type : "consulta"


<strong>POST</strong> - Save Visitor<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/visitor/<br>
*All parameter required<br>
<strong>Parameters:</strong>
- event_type : "llegada_visitante"
- visitor_name : ""
- document_type : "(cc, nit, ti)"
- document_number : ""
- document_number_employee : ""

<strong>POST</strong> - Update Visitor<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/visitor/<br>
*All parameter required<br>
<strong>Parameters:</strong>
- event_type : "salida_visitante"
- document_number : ""
- departure_time : "YYYY-MM-DD"

<br>

## Employees API
<strong>GET</strong> - Employee list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/employees/


<strong>POST</strong> - Save employee<br>
<strong>Endpoint:</strong> http://localhost:8000/api_firestore/employee/<br>
*All parameter required<br>
<strong>Parameters:</strong>
- document_type : (cc, nit, ti)
- document_number : ""

<br>


<br><br>
# For Sqlite

## Visitors API
<strong>GET</strong> - Visitor list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/visitors/

<strong>POST</strong> - Visitor list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/visitor/<br>
<strong>Parameters:</strong>
- event_type : "consulta"



<strong>POST</strong> - Save Visitor<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/visitor/<br>
<strong>Parameters:</strong>
- event_type : "llegada_visitante"
- visitor_name : ""
- document_type : "(cc, nit, ti)"
- document_number : ""
- document_number_employee : "" (required)



<strong>POST</strong> - Update Visitor<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/visitor/<br>
<strong>Parameters:</strong>
- event_type : "salida_visitante"
- document_number : ""
- departure_time : "YYYY-MM-DD"

<br>

## Employees API
<strong>GET</strong> - Employee list<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/employees/



<strong>POST</strong> - Save employee<br>
<strong>Endpoint:</strong> http://localhost:8000/api_sqlite/employee/<br>
<strong>Parameters:</strong>
- document_type : (cc, nit, ti)
- document_number : ""

<br>

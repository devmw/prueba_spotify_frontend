import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError} from 'rxjs';
import { catchError, tap, map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	key = ''
	items = []
	url = 'http://localhost:3000/get_a'

	constructor(private http: HttpClient) { }

	/**--------------------------------------------/
	*  -- FUNCION INICIAL PARA TRAER LA DATA --  
	*/
	ngOnInit() {
        this.http.get(this.url).subscribe((data) => {
        	this.items = data['data']
        });
	}

	/**--------------------------------------------/
	*  --  FUNCION PARA FILTRAR POR KEYWORK (BTN) --  
	*/
	Search_key(key) {
		this.items = []
        this.http.get(this.url+'/'+key).subscribe((data) => {
        	this.items = data['data']
        });
    }

    /**--------------------------------------------/
	*  -- FUNCION PARA PEDIR TODA LA DATA (BTN) --  
	*/
	Find_all(){
		this.items = []
		this.http.get(this.url).subscribe((data) => {
        	this.items = data['data']
        });
	}
}



